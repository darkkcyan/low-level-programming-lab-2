; vim: filetype=nasm
%include "colon.inc"

extern string_equals
global find_word

section .text

; paramters:
;   rdi: pointer to null-terminated string
;        to the searching word.
;   rsi: pointer to the linked list of the dictionary
; returns:
;   rax: 0 if word not found
;        pointer to the word's record if found
find_word:
    push rbx
    push rbp
    mov rbp, rdi
    mov rbx, rsi
.loop:    
    test rbx, rbx
    jz .done_searching
    mov rdi, rbp
    lea rsi, [rbx + COLON_WORD_STR_OFS]
    call string_equals
    test rax, rax
    jnz .done_searching
    mov rbx, [rbx]
    jmp .loop
    
.done_searching:
    mov rax, rbx
    pop rbp
    pop rbx
    ret
    
