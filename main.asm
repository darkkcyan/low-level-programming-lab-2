; vim: filetype=nasm
%include "words.inc"

extern exit
extern find_word
extern print_string.with_length
extern print_string
extern print_newline
extern read_word
extern string_length

section .data
msg:
    .ask_input: db "Enter your word (length's limit: 255): ", 0
error_msg:
    .word_too_long: db "Word's limit is only 255 characters!", 0
    .word_not_found: db "Word's not found", 0
control_sequence:           ; we need to be stylish :))
    .foreground_red: db 27, "[31m", 0
    .reset: db 27, "[0m", 0

%define MAX_STRING_LENGTH 256
input_word: times MAX_STRING_LENGTH db 0


section .text
global _start

_start:
    mov rdi, msg.ask_input
    call print_string
    
    mov rdi, input_word
    mov rsi, MAX_STRING_LENGTH - 1
    call read_word
    mov rdi, error_msg.word_too_long
    test rax, rax
    jz exit_with_error
    
    mov rdi, input_word
    mov rsi, DICT_LIST_POINTER
    call find_word
    mov rdi, error_msg.word_not_found
    test rax, rax
    jz exit_with_error
    
    push rax
    
    mov rdi, input_word
    call string_length
    
    pop rdi
    lea rdi, [rdi + rax + COLON_WORD_STR_OFS + 1]
    call print_string
    call print_newline
    
    mov rdi, 0
    jmp exit

print_to_stderr:
    push rdi
    call string_length
    mov rdx, rax
    mov rax, 1
    pop rsi
    mov rdi, 2
    syscall
    ret


exit_with_error:
    push rdi
    mov rdi, control_sequence.foreground_red
    call print_to_stderr
    pop rdi
    call print_to_stderr
    mov rdi, control_sequence.reset
    call print_to_stderr
    
    ; print a newline character
    mov rax, 1
    lea rsi, [rsp + 1]
    mov byte [rsi], 10
    mov rdx, 1
    mov rdi, 2
    syscall
    
    mov rdi, 1
    jmp exit
    
