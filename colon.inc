; vim: filetype=nasm

%xdefine DICT_LIST_POINTER 0
%define COLON_NEXT_WORD_OFS 0
%define COLON_WORD_STR_OFS 8

%macro colon 2
%2:
    dq DICT_LIST_POINTER
    db %1, 0
    %xdefine DICT_LIST_POINTER %2
%endmacro
