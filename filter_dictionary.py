#!/usr/bin/env python3
import csv
import sys
import random

argv = list(sys.argv[1:])
if len(argv) < 1:
    argv.append(100)
if len(argv) < 2:
    argv.append(20201001)

count_words, seed = int(argv[0]), int(argv[1])

random.seed(seed)

with open('dictionary.csv', 'r') as base_dictionary_file:
    all_words = list(map(lambda row: [row[0].lower(), row[2]], csv.reader(base_dictionary_file)))
    
filtered_words = sorted(random.sample(all_words, count_words))

filtered_dictionary_file = csv.writer(sys.stdout)
for word in filtered_words:
    filtered_dictionary_file.writerow(word)
