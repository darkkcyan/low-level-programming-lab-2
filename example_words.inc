; vim: filetype=nasm
%include "colon.inc"

section .data

colon "thirdword", third_word
db "third word explanation", 0

colon "secondword", second_word
db "second word explanation", 0

colon "firstword", first_word
db "first word explanation", 0
