#!/usr/bin/env python3
import csv
import sys

words_csv = csv.reader(sys.stdin)

print("; vi" + "m: filetype=nasm")  # Separate vi and m so vim doesn't actually read it
print("%include \"colon.inc\"")
print()

for idx, word in enumerate(words_csv):
    word[0] = word[0].lower()
    # label = word[0].trim().lower().replace(' ', '_') 
    label = "word_number_" + str(idx)
    print("colon \"{}\", {}".format(word[0], label))
    print("db \"{}\", 0".format(word[1]))
    print()

